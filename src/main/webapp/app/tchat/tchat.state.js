(function() {
    'use strict';

    angular
        .module('tpJavaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('tchat', {
            parent: 'app',
            url: '/tchat',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/tchat/tchat.html',
                    controller: 'TchatController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('tchat');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
