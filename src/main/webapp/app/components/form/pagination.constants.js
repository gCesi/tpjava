(function() {
    'use strict';

    angular
        .module('tpJavaApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
