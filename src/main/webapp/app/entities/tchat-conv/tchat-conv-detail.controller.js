(function() {
    'use strict';

    angular
        .module('tpJavaApp')
        .controller('TchatConvDetailController', TchatConvDetailController);

    TchatConvDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'TchatConv', 'User', 'TchatCanal'];

    function TchatConvDetailController($scope, $rootScope, $stateParams, entity, TchatConv, User, TchatCanal) {
        var vm = this;
        vm.tchatConv = entity;
        
        var unsubscribe = $rootScope.$on('tpJavaApp:tchatConvUpdate', function(event, result) {
            vm.tchatConv = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
