(function() {
    'use strict';
    angular
        .module('tpJavaApp')
        .factory('TchatConv', TchatConv);

    TchatConv.$inject = ['$resource', 'DateUtils'];

    function TchatConv ($resource, DateUtils) {
        var resourceUrl =  'api/tchat-convs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateSend = DateUtils.convertLocalDateFromServer(data.dateSend);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.dateSend = DateUtils.convertLocalDateToServer(data.dateSend);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.dateSend = DateUtils.convertLocalDateToServer(data.dateSend);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
