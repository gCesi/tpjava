(function() {
    'use strict';

    angular
        .module('tpJavaApp')
        .controller('TchatConvDialogController', TchatConvDialogController);

    TchatConvDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TchatConv', 'User', 'TchatCanal'];

    function TchatConvDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TchatConv, User, TchatCanal) {
        var vm = this;
        vm.tchatConv = entity;
        vm.users = User.query();
        vm.tchatcanals = TchatCanal.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        var onSaveSuccess = function (result) {
            $scope.$emit('tpJavaApp:tchatConvUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.tchatConv.id !== null) {
                TchatConv.update(vm.tchatConv, onSaveSuccess, onSaveError);
            } else {
                TchatConv.save(vm.tchatConv, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.dateSend = false;

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };
    }
})();
