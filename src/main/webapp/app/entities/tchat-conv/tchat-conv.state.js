(function() {
    'use strict';

    angular
        .module('tpJavaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('tchat-conv', {
            parent: 'entity',
            url: '/tchat-conv?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'tpJavaApp.tchatConv.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tchat-conv/tchat-convs.html',
                    controller: 'TchatConvController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tchatConv');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('tchat-conv-detail', {
            parent: 'entity',
            url: '/tchat-conv/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'tpJavaApp.tchatConv.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tchat-conv/tchat-conv-detail.html',
                    controller: 'TchatConvDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tchatConv');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TchatConv', function($stateParams, TchatConv) {
                    return TchatConv.get({id : $stateParams.id});
                }]
            }
        })
        .state('tchat-conv.new', {
            parent: 'tchat-conv',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tchat-conv/tchat-conv-dialog.html',
                    controller: 'TchatConvDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                message: null,
                                dateSend: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('tchat-conv', null, { reload: true });
                }, function() {
                    $state.go('tchat-conv');
                });
            }]
        })
        .state('tchat-conv.edit', {
            parent: 'tchat-conv',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tchat-conv/tchat-conv-dialog.html',
                    controller: 'TchatConvDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TchatConv', function(TchatConv) {
                            return TchatConv.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('tchat-conv', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tchat-conv.delete', {
            parent: 'tchat-conv',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tchat-conv/tchat-conv-delete-dialog.html',
                    controller: 'TchatConvDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TchatConv', function(TchatConv) {
                            return TchatConv.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('tchat-conv', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
