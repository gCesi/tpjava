(function() {
    'use strict';

    angular
        .module('tpJavaApp')
        .controller('TchatConvDeleteController',TchatConvDeleteController);

    TchatConvDeleteController.$inject = ['$uibModalInstance', 'entity', 'TchatConv'];

    function TchatConvDeleteController($uibModalInstance, entity, TchatConv) {
        var vm = this;
        vm.tchatConv = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            TchatConv.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
