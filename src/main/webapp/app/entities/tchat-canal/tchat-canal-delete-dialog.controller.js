(function() {
    'use strict';

    angular
        .module('tpJavaApp')
        .controller('TchatCanalDeleteController',TchatCanalDeleteController);

    TchatCanalDeleteController.$inject = ['$uibModalInstance', 'entity', 'TchatCanal'];

    function TchatCanalDeleteController($uibModalInstance, entity, TchatCanal) {
        var vm = this;
        vm.tchatCanal = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            TchatCanal.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
