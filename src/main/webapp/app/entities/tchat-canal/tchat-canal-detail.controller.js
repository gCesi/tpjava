(function() {
    'use strict';

    angular
        .module('tpJavaApp')
        .controller('TchatCanalDetailController', TchatCanalDetailController);

    TchatCanalDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'TchatCanal'];

    function TchatCanalDetailController($scope, $rootScope, $stateParams, entity, TchatCanal) {
        var vm = this;
        vm.tchatCanal = entity;
        
        var unsubscribe = $rootScope.$on('tpJavaApp:tchatCanalUpdate', function(event, result) {
            vm.tchatCanal = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
