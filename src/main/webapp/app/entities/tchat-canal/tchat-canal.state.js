(function() {
    'use strict';

    angular
        .module('tpJavaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('tchat-canal', {
            parent: 'entity',
            url: '/tchat-canal?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'tpJavaApp.tchatCanal.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tchat-canal/tchat-canals.html',
                    controller: 'TchatCanalController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tchatCanal');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('tchat-canal-detail', {
            parent: 'entity',
            url: '/tchat-canal/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'tpJavaApp.tchatCanal.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tchat-canal/tchat-canal-detail.html',
                    controller: 'TchatCanalDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tchatCanal');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TchatCanal', function($stateParams, TchatCanal) {
                    return TchatCanal.get({id : $stateParams.id});
                }]
            }
        })
        .state('tchat-canal.new', {
            parent: 'tchat-canal',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tchat-canal/tchat-canal-dialog.html',
                    controller: 'TchatCanalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                label: null,
                                dateCrea: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('tchat-canal', null, { reload: true });
                }, function() {
                    $state.go('tchat-canal');
                });
            }]
        })
        .state('tchat-canal.edit', {
            parent: 'tchat-canal',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tchat-canal/tchat-canal-dialog.html',
                    controller: 'TchatCanalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TchatCanal', function(TchatCanal) {
                            return TchatCanal.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('tchat-canal', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tchat-canal.delete', {
            parent: 'tchat-canal',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tchat-canal/tchat-canal-delete-dialog.html',
                    controller: 'TchatCanalDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TchatCanal', function(TchatCanal) {
                            return TchatCanal.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('tchat-canal', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
