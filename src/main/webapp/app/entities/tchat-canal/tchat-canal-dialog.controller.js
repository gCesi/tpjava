(function() {
    'use strict';

    angular
        .module('tpJavaApp')
        .controller('TchatCanalDialogController', TchatCanalDialogController);

    TchatCanalDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TchatCanal'];

    function TchatCanalDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TchatCanal) {
        var vm = this;
        vm.tchatCanal = entity;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        var onSaveSuccess = function (result) {
            $scope.$emit('tpJavaApp:tchatCanalUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.tchatCanal.id !== null) {
                TchatCanal.update(vm.tchatCanal, onSaveSuccess, onSaveError);
            } else {
                TchatCanal.save(vm.tchatCanal, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.dateCrea = false;

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };
    }
})();
