(function() {
    'use strict';
    angular
        .module('tpJavaApp')
        .factory('TchatCanal', TchatCanal);

    TchatCanal.$inject = ['$resource', 'DateUtils'];

    function TchatCanal ($resource, DateUtils) {
        var resourceUrl =  'api/tchat-canals/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateCrea = DateUtils.convertLocalDateFromServer(data.dateCrea);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.dateCrea = DateUtils.convertLocalDateToServer(data.dateCrea);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.dateCrea = DateUtils.convertLocalDateToServer(data.dateCrea);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
