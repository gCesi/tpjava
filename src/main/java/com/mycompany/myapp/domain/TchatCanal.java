package com.mycompany.myapp.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A TchatCanal.
 */
@Entity
@Table(name = "tchat_canal")
public class TchatCanal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "label", nullable = false)
    private String label;

    @NotNull
    @Column(name = "date_crea", nullable = false)
    private LocalDate dateCrea;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getDateCrea() {
        return dateCrea;
    }

    public void setDateCrea(LocalDate dateCrea) {
        this.dateCrea = dateCrea;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TchatCanal tchatCanal = (TchatCanal) o;
        if(tchatCanal.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tchatCanal.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TchatCanal{" +
            "id=" + id +
            ", label='" + label + "'" +
            ", dateCrea='" + dateCrea + "'" +
            '}';
    }
}
