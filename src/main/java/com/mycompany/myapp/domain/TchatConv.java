package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A TchatConv.
 */
@Entity
@Table(name = "tchat_conv")
public class TchatConv implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "message", nullable = false)
    private String message;

    @NotNull
    @Column(name = "date_send", nullable = false)
    private LocalDate dateSend;

    @ManyToOne
    private User fk_idUser;

    @OneToMany(mappedBy = "idCanal")
    @JsonIgnore
    private Set<TchatCanal> fk_idCanals = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDate getDateSend() {
        return dateSend;
    }

    public void setDateSend(LocalDate dateSend) {
        this.dateSend = dateSend;
    }

    public User getFk_idUser() {
        return fk_idUser;
    }

    public void setFk_idUser(User user) {
        this.fk_idUser = user;
    }

    public Set<TchatCanal> getFk_idCanals() {
        return fk_idCanals;
    }

    public void setFk_idCanals(Set<TchatCanal> tchatCanals) {
        this.fk_idCanals = tchatCanals;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TchatConv tchatConv = (TchatConv) o;
        if(tchatConv.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tchatConv.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TchatConv{" +
            "id=" + id +
            ", message='" + message + "'" +
            ", dateSend='" + dateSend + "'" +
            '}';
    }
}
