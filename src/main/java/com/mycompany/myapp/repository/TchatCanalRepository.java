package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.TchatCanal;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TchatCanal entity.
 */
@SuppressWarnings("unused")
public interface TchatCanalRepository extends JpaRepository<TchatCanal,Long> {

}
