package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.TchatConv;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TchatConv entity.
 */
@SuppressWarnings("unused")
public interface TchatConvRepository extends JpaRepository<TchatConv,Long> {

    @Query("select tchatConv from TchatConv tchatConv where tchatConv.fk_idUser.login = ?#{principal.username}")
    List<TchatConv> findByFk_idUserIsCurrentUser();

}
