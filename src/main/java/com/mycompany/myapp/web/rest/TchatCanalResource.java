package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.TchatCanal;
import com.mycompany.myapp.repository.TchatCanalRepository;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TchatCanal.
 */
@RestController
@RequestMapping("/api")
public class TchatCanalResource {

    private final Logger log = LoggerFactory.getLogger(TchatCanalResource.class);
        
    @Inject
    private TchatCanalRepository tchatCanalRepository;
    
    /**
     * POST  /tchat-canals : Create a new tchatCanal.
     *
     * @param tchatCanal the tchatCanal to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tchatCanal, or with status 400 (Bad Request) if the tchatCanal has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/tchat-canals",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TchatCanal> createTchatCanal(@Valid @RequestBody TchatCanal tchatCanal) throws URISyntaxException {
        log.debug("REST request to save TchatCanal : {}", tchatCanal);
        if (tchatCanal.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tchatCanal", "idexists", "A new tchatCanal cannot already have an ID")).body(null);
        }
        TchatCanal result = tchatCanalRepository.save(tchatCanal);
        return ResponseEntity.created(new URI("/api/tchat-canals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tchatCanal", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tchat-canals : Updates an existing tchatCanal.
     *
     * @param tchatCanal the tchatCanal to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tchatCanal,
     * or with status 400 (Bad Request) if the tchatCanal is not valid,
     * or with status 500 (Internal Server Error) if the tchatCanal couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/tchat-canals",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TchatCanal> updateTchatCanal(@Valid @RequestBody TchatCanal tchatCanal) throws URISyntaxException {
        log.debug("REST request to update TchatCanal : {}", tchatCanal);
        if (tchatCanal.getId() == null) {
            return createTchatCanal(tchatCanal);
        }
        TchatCanal result = tchatCanalRepository.save(tchatCanal);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tchatCanal", tchatCanal.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tchat-canals : get all the tchatCanals.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tchatCanals in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/tchat-canals",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TchatCanal>> getAllTchatCanals(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TchatCanals");
        Page<TchatCanal> page = tchatCanalRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tchat-canals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tchat-canals/:id : get the "id" tchatCanal.
     *
     * @param id the id of the tchatCanal to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tchatCanal, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/tchat-canals/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TchatCanal> getTchatCanal(@PathVariable Long id) {
        log.debug("REST request to get TchatCanal : {}", id);
        TchatCanal tchatCanal = tchatCanalRepository.findOne(id);
        return Optional.ofNullable(tchatCanal)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tchat-canals/:id : delete the "id" tchatCanal.
     *
     * @param id the id of the tchatCanal to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/tchat-canals/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTchatCanal(@PathVariable Long id) {
        log.debug("REST request to delete TchatCanal : {}", id);
        tchatCanalRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tchatCanal", id.toString())).build();
    }

}
