package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.TchatConv;
import com.mycompany.myapp.repository.TchatConvRepository;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TchatConv.
 */
@RestController
@RequestMapping("/api")
public class TchatConvResource {

    private final Logger log = LoggerFactory.getLogger(TchatConvResource.class);
        
    @Inject
    private TchatConvRepository tchatConvRepository;
    
    /**
     * POST  /tchat-convs : Create a new tchatConv.
     *
     * @param tchatConv the tchatConv to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tchatConv, or with status 400 (Bad Request) if the tchatConv has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/tchat-convs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TchatConv> createTchatConv(@Valid @RequestBody TchatConv tchatConv) throws URISyntaxException {
        log.debug("REST request to save TchatConv : {}", tchatConv);
        if (tchatConv.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tchatConv", "idexists", "A new tchatConv cannot already have an ID")).body(null);
        }
        TchatConv result = tchatConvRepository.save(tchatConv);
        return ResponseEntity.created(new URI("/api/tchat-convs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tchatConv", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tchat-convs : Updates an existing tchatConv.
     *
     * @param tchatConv the tchatConv to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tchatConv,
     * or with status 400 (Bad Request) if the tchatConv is not valid,
     * or with status 500 (Internal Server Error) if the tchatConv couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/tchat-convs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TchatConv> updateTchatConv(@Valid @RequestBody TchatConv tchatConv) throws URISyntaxException {
        log.debug("REST request to update TchatConv : {}", tchatConv);
        if (tchatConv.getId() == null) {
            return createTchatConv(tchatConv);
        }
        TchatConv result = tchatConvRepository.save(tchatConv);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tchatConv", tchatConv.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tchat-convs : get all the tchatConvs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tchatConvs in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/tchat-convs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TchatConv>> getAllTchatConvs(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TchatConvs");
        Page<TchatConv> page = tchatConvRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tchat-convs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tchat-convs/:id : get the "id" tchatConv.
     *
     * @param id the id of the tchatConv to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tchatConv, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/tchat-convs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TchatConv> getTchatConv(@PathVariable Long id) {
        log.debug("REST request to get TchatConv : {}", id);
        TchatConv tchatConv = tchatConvRepository.findOne(id);
        return Optional.ofNullable(tchatConv)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tchat-convs/:id : delete the "id" tchatConv.
     *
     * @param id the id of the tchatConv to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/tchat-convs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTchatConv(@PathVariable Long id) {
        log.debug("REST request to delete TchatConv : {}", id);
        tchatConvRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tchatConv", id.toString())).build();
    }

}
