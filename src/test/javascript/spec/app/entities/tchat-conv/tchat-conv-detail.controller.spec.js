'use strict';

describe('Controller Tests', function() {

    describe('TchatConv Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTchatConv, MockUser, MockTchatCanal;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTchatConv = jasmine.createSpy('MockTchatConv');
            MockUser = jasmine.createSpy('MockUser');
            MockTchatCanal = jasmine.createSpy('MockTchatCanal');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'TchatConv': MockTchatConv,
                'User': MockUser,
                'TchatCanal': MockTchatCanal
            };
            createController = function() {
                $injector.get('$controller')("TchatConvDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'tpJavaApp:tchatConvUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
