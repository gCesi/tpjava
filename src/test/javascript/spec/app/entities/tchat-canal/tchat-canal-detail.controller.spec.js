'use strict';

describe('Controller Tests', function() {

    describe('TchatCanal Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTchatCanal;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTchatCanal = jasmine.createSpy('MockTchatCanal');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'TchatCanal': MockTchatCanal
            };
            createController = function() {
                $injector.get('$controller')("TchatCanalDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'tpJavaApp:tchatCanalUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
