package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TpJavaApp;
import com.mycompany.myapp.domain.TchatCanal;
import com.mycompany.myapp.repository.TchatCanalRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TchatCanalResource REST controller.
 *
 * @see TchatCanalResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TpJavaApp.class)
@WebAppConfiguration
@IntegrationTest
public class TchatCanalResourceIntTest {

    private static final String DEFAULT_LABEL = "AAAAA";
    private static final String UPDATED_LABEL = "BBBBB";

    private static final LocalDate DEFAULT_DATE_CREA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_CREA = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private TchatCanalRepository tchatCanalRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTchatCanalMockMvc;

    private TchatCanal tchatCanal;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TchatCanalResource tchatCanalResource = new TchatCanalResource();
        ReflectionTestUtils.setField(tchatCanalResource, "tchatCanalRepository", tchatCanalRepository);
        this.restTchatCanalMockMvc = MockMvcBuilders.standaloneSetup(tchatCanalResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tchatCanal = new TchatCanal();
        tchatCanal.setLabel(DEFAULT_LABEL);
        tchatCanal.setDateCrea(DEFAULT_DATE_CREA);
    }

    @Test
    @Transactional
    public void createTchatCanal() throws Exception {
        int databaseSizeBeforeCreate = tchatCanalRepository.findAll().size();

        // Create the TchatCanal

        restTchatCanalMockMvc.perform(post("/api/tchat-canals")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tchatCanal)))
                .andExpect(status().isCreated());

        // Validate the TchatCanal in the database
        List<TchatCanal> tchatCanals = tchatCanalRepository.findAll();
        assertThat(tchatCanals).hasSize(databaseSizeBeforeCreate + 1);
        TchatCanal testTchatCanal = tchatCanals.get(tchatCanals.size() - 1);
        assertThat(testTchatCanal.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testTchatCanal.getDateCrea()).isEqualTo(DEFAULT_DATE_CREA);
    }

    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = tchatCanalRepository.findAll().size();
        // set the field null
        tchatCanal.setLabel(null);

        // Create the TchatCanal, which fails.

        restTchatCanalMockMvc.perform(post("/api/tchat-canals")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tchatCanal)))
                .andExpect(status().isBadRequest());

        List<TchatCanal> tchatCanals = tchatCanalRepository.findAll();
        assertThat(tchatCanals).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateCreaIsRequired() throws Exception {
        int databaseSizeBeforeTest = tchatCanalRepository.findAll().size();
        // set the field null
        tchatCanal.setDateCrea(null);

        // Create the TchatCanal, which fails.

        restTchatCanalMockMvc.perform(post("/api/tchat-canals")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tchatCanal)))
                .andExpect(status().isBadRequest());

        List<TchatCanal> tchatCanals = tchatCanalRepository.findAll();
        assertThat(tchatCanals).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTchatCanals() throws Exception {
        // Initialize the database
        tchatCanalRepository.saveAndFlush(tchatCanal);

        // Get all the tchatCanals
        restTchatCanalMockMvc.perform(get("/api/tchat-canals?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tchatCanal.getId().intValue())))
                .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL.toString())))
                .andExpect(jsonPath("$.[*].dateCrea").value(hasItem(DEFAULT_DATE_CREA.toString())));
    }

    @Test
    @Transactional
    public void getTchatCanal() throws Exception {
        // Initialize the database
        tchatCanalRepository.saveAndFlush(tchatCanal);

        // Get the tchatCanal
        restTchatCanalMockMvc.perform(get("/api/tchat-canals/{id}", tchatCanal.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tchatCanal.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL.toString()))
            .andExpect(jsonPath("$.dateCrea").value(DEFAULT_DATE_CREA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTchatCanal() throws Exception {
        // Get the tchatCanal
        restTchatCanalMockMvc.perform(get("/api/tchat-canals/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTchatCanal() throws Exception {
        // Initialize the database
        tchatCanalRepository.saveAndFlush(tchatCanal);
        int databaseSizeBeforeUpdate = tchatCanalRepository.findAll().size();

        // Update the tchatCanal
        TchatCanal updatedTchatCanal = new TchatCanal();
        updatedTchatCanal.setId(tchatCanal.getId());
        updatedTchatCanal.setLabel(UPDATED_LABEL);
        updatedTchatCanal.setDateCrea(UPDATED_DATE_CREA);

        restTchatCanalMockMvc.perform(put("/api/tchat-canals")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedTchatCanal)))
                .andExpect(status().isOk());

        // Validate the TchatCanal in the database
        List<TchatCanal> tchatCanals = tchatCanalRepository.findAll();
        assertThat(tchatCanals).hasSize(databaseSizeBeforeUpdate);
        TchatCanal testTchatCanal = tchatCanals.get(tchatCanals.size() - 1);
        assertThat(testTchatCanal.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testTchatCanal.getDateCrea()).isEqualTo(UPDATED_DATE_CREA);
    }

    @Test
    @Transactional
    public void deleteTchatCanal() throws Exception {
        // Initialize the database
        tchatCanalRepository.saveAndFlush(tchatCanal);
        int databaseSizeBeforeDelete = tchatCanalRepository.findAll().size();

        // Get the tchatCanal
        restTchatCanalMockMvc.perform(delete("/api/tchat-canals/{id}", tchatCanal.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TchatCanal> tchatCanals = tchatCanalRepository.findAll();
        assertThat(tchatCanals).hasSize(databaseSizeBeforeDelete - 1);
    }
}
