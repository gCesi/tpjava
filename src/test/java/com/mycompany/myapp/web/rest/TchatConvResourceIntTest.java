package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TpJavaApp;
import com.mycompany.myapp.domain.TchatConv;
import com.mycompany.myapp.repository.TchatConvRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TchatConvResource REST controller.
 *
 * @see TchatConvResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TpJavaApp.class)
@WebAppConfiguration
@IntegrationTest
public class TchatConvResourceIntTest {

    private static final String DEFAULT_MESSAGE = "AAAAA";
    private static final String UPDATED_MESSAGE = "BBBBB";

    private static final LocalDate DEFAULT_DATE_SEND = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_SEND = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private TchatConvRepository tchatConvRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTchatConvMockMvc;

    private TchatConv tchatConv;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TchatConvResource tchatConvResource = new TchatConvResource();
        ReflectionTestUtils.setField(tchatConvResource, "tchatConvRepository", tchatConvRepository);
        this.restTchatConvMockMvc = MockMvcBuilders.standaloneSetup(tchatConvResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tchatConv = new TchatConv();
        tchatConv.setMessage(DEFAULT_MESSAGE);
        tchatConv.setDateSend(DEFAULT_DATE_SEND);
    }

    @Test
    @Transactional
    public void createTchatConv() throws Exception {
        int databaseSizeBeforeCreate = tchatConvRepository.findAll().size();

        // Create the TchatConv

        restTchatConvMockMvc.perform(post("/api/tchat-convs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tchatConv)))
                .andExpect(status().isCreated());

        // Validate the TchatConv in the database
        List<TchatConv> tchatConvs = tchatConvRepository.findAll();
        assertThat(tchatConvs).hasSize(databaseSizeBeforeCreate + 1);
        TchatConv testTchatConv = tchatConvs.get(tchatConvs.size() - 1);
        assertThat(testTchatConv.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testTchatConv.getDateSend()).isEqualTo(DEFAULT_DATE_SEND);
    }

    @Test
    @Transactional
    public void checkMessageIsRequired() throws Exception {
        int databaseSizeBeforeTest = tchatConvRepository.findAll().size();
        // set the field null
        tchatConv.setMessage(null);

        // Create the TchatConv, which fails.

        restTchatConvMockMvc.perform(post("/api/tchat-convs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tchatConv)))
                .andExpect(status().isBadRequest());

        List<TchatConv> tchatConvs = tchatConvRepository.findAll();
        assertThat(tchatConvs).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateSendIsRequired() throws Exception {
        int databaseSizeBeforeTest = tchatConvRepository.findAll().size();
        // set the field null
        tchatConv.setDateSend(null);

        // Create the TchatConv, which fails.

        restTchatConvMockMvc.perform(post("/api/tchat-convs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tchatConv)))
                .andExpect(status().isBadRequest());

        List<TchatConv> tchatConvs = tchatConvRepository.findAll();
        assertThat(tchatConvs).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTchatConvs() throws Exception {
        // Initialize the database
        tchatConvRepository.saveAndFlush(tchatConv);

        // Get all the tchatConvs
        restTchatConvMockMvc.perform(get("/api/tchat-convs?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tchatConv.getId().intValue())))
                .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
                .andExpect(jsonPath("$.[*].dateSend").value(hasItem(DEFAULT_DATE_SEND.toString())));
    }

    @Test
    @Transactional
    public void getTchatConv() throws Exception {
        // Initialize the database
        tchatConvRepository.saveAndFlush(tchatConv);

        // Get the tchatConv
        restTchatConvMockMvc.perform(get("/api/tchat-convs/{id}", tchatConv.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tchatConv.getId().intValue()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()))
            .andExpect(jsonPath("$.dateSend").value(DEFAULT_DATE_SEND.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTchatConv() throws Exception {
        // Get the tchatConv
        restTchatConvMockMvc.perform(get("/api/tchat-convs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTchatConv() throws Exception {
        // Initialize the database
        tchatConvRepository.saveAndFlush(tchatConv);
        int databaseSizeBeforeUpdate = tchatConvRepository.findAll().size();

        // Update the tchatConv
        TchatConv updatedTchatConv = new TchatConv();
        updatedTchatConv.setId(tchatConv.getId());
        updatedTchatConv.setMessage(UPDATED_MESSAGE);
        updatedTchatConv.setDateSend(UPDATED_DATE_SEND);

        restTchatConvMockMvc.perform(put("/api/tchat-convs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedTchatConv)))
                .andExpect(status().isOk());

        // Validate the TchatConv in the database
        List<TchatConv> tchatConvs = tchatConvRepository.findAll();
        assertThat(tchatConvs).hasSize(databaseSizeBeforeUpdate);
        TchatConv testTchatConv = tchatConvs.get(tchatConvs.size() - 1);
        assertThat(testTchatConv.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testTchatConv.getDateSend()).isEqualTo(UPDATED_DATE_SEND);
    }

    @Test
    @Transactional
    public void deleteTchatConv() throws Exception {
        // Initialize the database
        tchatConvRepository.saveAndFlush(tchatConv);
        int databaseSizeBeforeDelete = tchatConvRepository.findAll().size();

        // Get the tchatConv
        restTchatConvMockMvc.perform(delete("/api/tchat-convs/{id}", tchatConv.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TchatConv> tchatConvs = tchatConvRepository.findAll();
        assertThat(tchatConvs).hasSize(databaseSizeBeforeDelete - 1);
    }
}
